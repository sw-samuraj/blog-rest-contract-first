# Contract-first REST service via Swagger & Gradle #

An example project for demonstration of the contract-first approach
for REST web services. It's implemented via help of [Swagger](http://swagger.io/)
and [Gradle](https://gradle.org/).

## How to run the example

1. Clone the repository.
1. Run the command `gradle jettyRun`.
1. Open a browser on URL <http://localhost:4040/deep-thought/question>
1. See the JSON response in the browser.
1. Browse the source code.

## What if you're sitting behind a proxy?

You need to modify (or create) your `$HOME/.gradle/gradle.properties` in
following way:

```
#!properties
systemProp.http.proxyHost=<your-proxy-host>
systemProp.http.proxyPort=<your-proxy-port>
systemProp.http.nonProxyHosts=localhost|<other-non-proxy-host>
systemProp.https.proxyHost=<your-proxy-host>
systemProp.https.proxyPort=<your-proxy-port>
systemProp.https.nonProxyHosts=localhost|<other-non-proxy-host>
```

## License ##

The **blog-rest-contract-first** project is published under [BSD 3-Clause](http://opensource.org/licenses/BSD-3-Clause) license.
