package cz.swsamuraj.rest.contractfirst.ws.api;

import cz.swsamuraj.rest.contractfirst.ws.model.Answer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

@Controller
public class QuestionApiController implements QuestionApi {

    @Override
    public ResponseEntity<Answer> getAnswer() {
        Answer answer = new Answer();
        answer.setAnswer(42);

        return new ResponseEntity<Answer>(answer, HttpStatus.OK);
    }
}
